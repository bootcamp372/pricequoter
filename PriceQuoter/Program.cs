﻿using System.Diagnostics;
using System.Linq.Expressions;

namespace PriceQuoter
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.Write("Enter GET-QUOTE to get a quote or QUIT to quit: ");
            string command = Console.ReadLine();

            while (command == "GET-QUOTE")
            {
 
                

                    string productCode;
                    Console.Write("Enter a product code: ");
                    productCode = Console.ReadLine();

                    double quantity;
                    Console.Write("Enter a quantity: ");
                    quantity = Convert.ToDouble(Console.ReadLine());

                    GetPricesForProduct(productCode, quantity);

                Console.Write("Enter GET-QUOTE to get a quote or QUIT to quit: ");
                command = Console.ReadLine();
                if (command == "GET-QUOTE")
                {
                    GetPricesForProduct(productCode, quantity);
                }
                if (command == "QUIT")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("**Error: unrecognized command");
                }
            }
        }

        static void GetPricesForProduct(string productCode, double quantity) {

            double highPrice;
            double medPrice;
            double lowPrice;


            double totalPrice;
            double unitPrice;
            double discount = (quantity >= 250) ? 0.15 : 0;
            switch (productCode)

            {

                case "BG-127":
                    highPrice = 18.99;
                    medPrice = 17.00;
                    lowPrice = 14.49;
                    unitPrice = GetPrice(lowPrice, medPrice, highPrice, quantity, discount);
                    totalPrice = unitPrice * quantity;
                    totalPrice += totalPrice * discount;

                    Console.WriteLine($"Product Code: {productCode}");
                    Console.WriteLine($"Unit Price: {unitPrice:C}");
                    Console.WriteLine($"Discount: {discount:P}");
                    Console.WriteLine($"Total Price: {totalPrice:C}");

                    break;
                case "WRTR-28":
                    highPrice = 125.00;
                    medPrice = 113.75;
                    lowPrice = 99.99;
                    unitPrice = GetPrice(lowPrice, medPrice, highPrice, quantity, discount);
                    totalPrice = unitPrice * quantity;
                    totalPrice += totalPrice * discount;

                    Console.WriteLine($"Product Code: {productCode}");
                    Console.WriteLine($"Unit Price: {unitPrice:C}");
                    Console.WriteLine($"Discount: {discount:P}");
                    Console.WriteLine($"Total Price: {totalPrice:C}");
                    break;
                case "GUAC-8":
                    highPrice = 8.99;
                    medPrice = 8.99;
                    lowPrice = 7.49;
                    unitPrice = GetPrice(lowPrice, medPrice, highPrice, quantity, discount);
                    totalPrice = unitPrice * quantity;
                    totalPrice += totalPrice * discount;

                    Console.WriteLine($"Product Code: {productCode}");
                    Console.WriteLine($"Unit Price: {unitPrice:C}");
                    Console.WriteLine($"Discount: {discount:P}");
                    Console.WriteLine($"Total Price: {totalPrice:C}");
                    break;
                default:
                    Console.WriteLine("**Error: unrecognized product code");
                    break;

            }

        }

        public static double GetPrice(double price1, double price2, double price3, double quantity, double discount)
        {
            double price;


            if ((1 <= quantity && quantity <= 24))
            {
                price = price3;
            }
            else if (25 <= quantity && quantity <= 50)
            {
                price = price2;
            }
            else if (quantity >= 51)
            {
                price = price1;
            }
            else
            {
                price = 0;
            }
            return price;
        }

    }
}