﻿namespace GregorianCalendar
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double quantity;
            Console.Write("Enter a year: ");
            double year = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Hello, World!");



            if (year % 4 == 0)
            {
                if (year % 100 == 0)
                {
                    if (year % 400 == 0)
                    {
                        Console.WriteLine($"This is a leap year: {year}");
                    }
                    else
                    {
                        Console.WriteLine($"This is not a leap year: {year}");
                    }
                }
                else
                {
                    Console.WriteLine($"This is a leap year: {year}");
                }
            }
        }
    }
}